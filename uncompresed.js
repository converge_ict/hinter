if (typeof jQuery == 'undefined') {

   function getScript(url, success) {

      var script     = document.createElement('script');
           script.src = url;

      var head = document.getElementsByTagName('head')[0],
      done = false;

      // Attach handlers for all browsers
      script.onload = script.onreadystatechange = function() {

         if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {

         done = true;

            // callback function provided as param
            success();

            script.onload = script.onreadystatechange = null;
            head.removeChild(script);

         };

      };

      head.appendChild(script);

   };

   getScript('http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js', function() {});

}

jQuery(function(){

    var cvg_control_key = false;

    jQuery(document).on('keyup keydown', function(e) {
        cvg_control_key = e.ctrlKey;
        console.log(cvg_control_key ? 'ON' : 'OFF');
    });

    jQuery(document).on('click', '[block-hinter=\"true\"]', function(e){
        if(cvg_control_key){
          cvg_control_key = false;
          var that = jQuery(this);
          var all  = jQuery('[block-template=\"'+that.attr('block-template')+'\"]');
          e.preventDefault();
          e.stopPropagation();
          all.addClass('cvghinter');
          setTimeout(function(){ all.removeClass('cvghinter') },2000);
          alert(that.attr('block-template'));
          console.log(that.attr('block-template'));
          console.warn(that.attr('block-class'));
        }
    });

});