# README #

Hinter Module.

### What is this repository for? ###

Hinter Module allows you to see more hints in Magento's Layout.

### Installation Instructions ###


1. Enable "Allow Symlinks" (System > Configuration > Advanced > Developer)
2. Install modman ``` bash < <(wget -q --no-check-certificate -O - https://raw.github.com/colinmollenhour/modman/master/modman-installer) ```
3. Switch to Magento root folder
4. ``` modman init ```
5. ``` modman clone https://convergeict@bitbucket.org/converge_ict/hinter.git ```
6. Clear cache
7. Enable Module from System->Configuration TAB:Advanced -> Hinter -> Enabled=Yes