<?php


class Converge_Hinter_Model_Observer
{
    public function coreBlockAbstractToHtmlAfter($observer)
    {
        $status = Mage::getStoreConfig('converge_hinter/hinter/enable');
        $dblckick = Mage::getStoreConfig('converge_hinter/hinter/dblckick');

        $param = Mage::getStoreConfig('converge_hinter/hinter/param');

        $_param = Mage::app()->getRequest()->getParam('hinter');

        if((boolean) $status && (strlen(trim($param)) == 0 || $_param == 1) ):
            $normalOutput   = $observer->getTransport()->getHtml();
            $template_name  = $observer->getBlock()->getTemplateFile();
            
            if(strlen(trim($template_name)) == 0 && !is_null($observer->getBlock()->getData('cache_tags'))):
                $id = preg_replace('/^cms_page_(\d+)$/', '$1', array_shift(array_slice($observer->getBlock()->getData('cache_tags'), 0, 1)));
                $page = Mage::getModel('cms/page')->load($id);
                
                $template_name  = "CMS Page: '" . $page->getData('identifier') . "'";
            endif;
            
            
            $normalOutput   = preg_replace(
                '/(?<=[^\']\<)(\w+?)([ \>])(?!block-template)/',
                '$1 block-template=\'' . $template_name . '\' block-hinter=\'true\' block-class=\'' . get_class($observer->getBlock()) . '\'$2',
                $normalOutput
            );
            
            if(preg_match('/\/head\.phtml$/', $template_name) && (boolean) $dblckick)
                $normalOutput .= "<style>.cvghinter{ background-color: rgba(240,128,128,1)!important }</style><script>function getScript(e,t){var a=document.createElement(\"script\");a.src=e;var o=document.getElementsByTagName(\"head\")[0],n=!1;a.onload=a.onreadystatechange=function(){n||this.readyState&&\"loaded\"!=this.readyState&&\"complete\"!=this.readyState||(n=!0,t(),a.onload=a.onreadystatechange=null,o.removeChild(a))},o.appendChild(a)}\"undefined\"==typeof jQuery&&getScript(\"http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js\",function(){}),jQuery(function(){var e=!1;jQuery(document).on(\"keyup keydown\",function(t){e=t.ctrlKey,console.log(e?\"ON\":\"OFF\")}),jQuery(document).on(\"click\",'[block-hinter=\"true\"]',function(t){if(e){e=!1;var a=jQuery(this),o=jQuery('[block-template=\"'+a.attr(\"block-template\")+'\"]');t.preventDefault(),t.stopPropagation(),o.addClass(\"cvghinter\"),setTimeout(function(){o.removeClass(\"cvghinter\")},2e3),alert(a.attr(\"block-template\")),console.log(a.attr(\"block-template\")),console.warn(a.attr(\"block-class\"))}})});</script>";

            $observer->getTransport()->setHtml($normalOutput);
        endif;
    }
}